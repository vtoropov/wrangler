/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jan-2021 at 10:12:34.995 pm, UTC+7, Novosibirsk, Friday;
	This is generic stroller-tramp-voyager data interface implementation file.
*/
#include "StdAfx.h"
#include "stroller_data.h"
#include "stroller_iface.h"

using namespace wanderer;

/////////////////////////////////////////////////////////////////////////////

CNumber_U:: CNumber_U (void) : m_value (0) {}
CNumber_U:: CNumber_U (const CNumber_U& _ref) : CNumber_U() { *this = _ref; }
CNumber_U:: CNumber_U (const UINT _value) : CNumber_U() { *this << _value; }
CNumber_U::~CNumber_U (void) {}

/////////////////////////////////////////////////////////////////////////////

BYTE   CNumber_U::Capacity (void) const { static CStringW cs_buffer; cs_buffer.Format(_T("%u"), m_value); return static_cast<BYTE>(cs_buffer.GetLength()); }
BYTE   CNumber_U::CapacityE(void) const {
	return (
		m_value < 10 ? 1 :( // ;D
			m_value < 100 ? 2 :(
				m_value < 1000 ? 3 :(
					m_value < 10000 ? 4 :(
						m_value < 100000 ? 5 :(
							m_value < 1000000 ? 6 :(
								m_value < 10000000 ? 7 :(
									m_value < 100000000 ? 8 : 1000000000 ? 9 : 10))))))));
}

UINT   CNumber_U::Value    (void) const { return m_value; }
UINT&  CNumber_U::Value    (void)       { return m_value; }

/////////////////////////////////////////////////////////////////////////////

CNumber_U::operator UINT   (void) const { return m_value; }

/////////////////////////////////////////////////////////////////////////////

CNumber_U& CNumber_U::operator = (const CNumber_U& _ref ) { *this << _ref.Value(); return *this; }
CNumber_U& CNumber_U::operator <<(const UINT _value) { this->Value() = _value; return *this; }

/////////////////////////////////////////////////////////////////////////////

CNode_U:: CNode_U (void) : m_x(0), m_y(0) {}
CNode_U:: CNode_U (const CNode_U& _ref) : CNode_U() { *this = _ref; }
CNode_U:: CNode_U (const LONG _x, const LONG _y) : m_x(_x), m_y(_y) {}
CNode_U::~CNode_U (void) {}

/////////////////////////////////////////////////////////////////////////////

LONG   CNode_U::X (void) const { return m_x; }
LONG&  CNode_U::X (void)       { return m_x; }
LONG   CNode_U::Y (void) const { return m_y; }
LONG&  CNode_U::Y (void)       { return m_y; }
LONG   CNode_U::Z (void) const { return (CDigit_Sum() >> *this); }

/////////////////////////////////////////////////////////////////////////////
#if defined(_DEBUG)
CStringW CNode_U::Print (void) const { CStringW cs_out; cs_out.Format(_T("x=%d;y=%d;z=%d"), this->X(), this->Y(), this->Z()); return cs_out; }
#endif
/////////////////////////////////////////////////////////////////////////////

CNode_U& CNode_U::operator = (const CNode_U& _ref) { this->X() = _ref.X(); this->Y() = _ref.Y(); return *this; }

/////////////////////////////////////////////////////////////////////////////

const bool CNode_U::operator < (const CNode_U& _rh) const {
	return ((this->X() <= _rh.X() && this->Y() < _rh.Y()) || (this->X() < _rh.X() && this->Y() <= _rh.Y()));
}
const bool CNode_U::operator ==(const CNode_U& _rh) const {
	return (this->X() == _rh.X() && this->Y() == _rh.Y());
}
const bool CNode_U::operator > (const CNode_U& _rh) const {
	return ((this->X() >= _rh.X() && this->Y() > _rh.Y()) || (this->X() > _rh.X() && this->Y() >= _rh.Y()));
}
const bool CNode_U::operator!= (const CNode_U& _rh) const {
	return (this->X() != _rh.X() || this->Y() != _rh.Y());
}

/////////////////////////////////////////////////////////////////////////////
#if (0)
namespace wanderer {
bool operator == (const CNode_U& _lh, const CNode_U& _rh) { return (_lh.operator==(_rh)); }
bool operator != (const CNode_U& _lh, const CNode_U& _rh) { return (_lh.operator!=(_rh)); }
bool operator <= (const CNode_U& _lh, const CNode_U& _rh) { return (_lh.operator< (_rh) || _lh.operator==(_rh)); }
bool operator >= (const CNode_U& _lh, const CNode_U& _rh) { return (_lh.operator> (_rh) || _lh.operator==(_rh)); }
bool operator >  (const CNode_U& _lh, const CNode_U& _rh) { return (_lh.operator> (_rh)); }
bool operator <  (const CNode_U& _lh, const CNode_U& _rh) { return (_lh.operator< (_rh)); }
}
#endif
/////////////////////////////////////////////////////////////////////////////

CRange:: CRange (void) {}
CRange:: CRange (const CRange& _ref) : CRange() { *this = _ref; }
CRange:: CRange (const CNode_U& _min, const CNode_U& _max) : CRange() { this->Max() = _max; this->Min() = _min; }
CRange::~CRange (void) {}

/////////////////////////////////////////////////////////////////////////////

bool     CRange::Is  (void) const { return (m_min.operator<(m_max)); } // TODO: comporison operators must be reviewed;
const
CNode_U& CRange::Max (void) const { return m_max; }
CNode_U& CRange::Max (void)       { return m_max; }
const
CNode_U& CRange::Min (void) const { return m_min; }
CNode_U& CRange::Min (void)       { return m_min; }

/////////////////////////////////////////////////////////////////////////////
#if defined(_DEBUG)
CStringW CRange::Print (void) const {
	CStringW cs_out; cs_out.Format(_T("min: %s; max: %s;"), (LPCWSTR)this->Min().Print(), (LPCWSTR)this->Max().Print()); return cs_out;
}
#endif
/////////////////////////////////////////////////////////////////////////////

bool     CRange::Include (const CNode_U& _node) const {
	const bool  b_x = (m_min.X() <= _node.X() && _node.X() <= m_max.X());
	const bool  b_y = (m_min.Y() <= _node.Y() && _node.Y() <= m_max.Y());
	return (b_x && b_y);
}

/////////////////////////////////////////////////////////////////////////////

CRange&  CRange::operator = (const CRange& _ref) { this->Max() = _ref.Max(); this->Min() = _ref.Min(); return *this; }