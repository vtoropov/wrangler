/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Aug-2016 at 6:20:45a, GMT+7, Phuket, Rawai, Tuesday;
	This is Evalu8 Outlook add-in precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to Fake GPS geo-location api project on 06-Mar-2020 at 2:27:36a, UTC+7, Novosibirsk, Monday;
	Adopted to Wanderer test project on 6-Jan-2021 at 4:22:35.873 pm, UTC+7, Novosibirsk, Tuesday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)
