/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jan-2021 at 7:46:42.040 pm, UTC+7, Novosibirsk, Tuesday;
	This is generic stroller-tramp-voyager path discovering interface implementation file.
*/
#include "StdAfx.h"
#include "stroller_iface.h"

using namespace wanderer;

/////////////////////////////////////////////////////////////////////////////

namespace wanderer { namespace _impl {

	UINT  Get_Max (UINT _n_capa, UINT _sum, LONG _from ) { // {_n_capa(city}|digit sum|start point/value);
		_n_capa; _sum, _from;
		const LONG u_sum = (CDigit_Sum() >> (_from + 1));
		if (u_sum > static_cast<LONG>(_sum))
			return _from; // no way to foreward or for increasing;

		UINT n_delta = 0;

		UINT n_fac = static_cast<UINT>(::std::pow(10, (_n_capa - 1)));
		
		_sum -= _from / n_fac;
#if (0)
		for (UINT i_ = _n_digits; i_ > 0 && _sum > 0; --i_) {
			if (9 < _sum) {
				n_max += 9 * (_n_digits - i_ == 0 ? 1  :  (_n_digits - i_) * 10); _sum -= 9;
			}
			else {
				n_max += (_sum + 1) *  static_cast<UINT>(::std::pow(10, (_n_digits - i_))); n_max -= 1; _sum = 0;
			}
		}
#else
		UINT i_ = 0;
		while (_sum) {
			if (9 < _sum) {
				n_delta += 9 * static_cast<UINT>(::std::pow(10, i_)); _sum -= 9;
			}
			else {
				n_delta += (_sum + 1) *  static_cast<UINT>(::std::pow(10, i_)); n_delta -= 1; _sum = 0;
			}
			++i_;
		};
#endif

		return n_delta + (_from / n_fac) * n_fac;
	}

	LONG  Get_Min (UINT _n_capa, UINT _sum, LONG _from ) {
		_n_capa;  _sum; _from;
		const LONG u_sum = ((_from - 1) < 0 ? CDigit_Sum() << (_from - 1) : CDigit_Sum() >> (_from - 1));
		if (u_sum > static_cast<LONG>(_sum))
			return _from; // no way back or for decreasing;

		UINT ndx_ = 2;
		while ((CDigit_Sum() << (_from - ndx_)) <= (LONG)_sum) {
			ndx_ += 1;
		}

		return (_from - ndx_ + 1);
	}
}}
using namespace wanderer::_impl;
/////////////////////////////////////////////////////////////////////////////

CError:: CError (void) : m_desc(_T("#no_error")), m_result(S_OK) {}
CError:: CError (const CError& _ref) : CError() { *this = _ref; }
CError:: CError (const HRESULT _hr, LPCWSTR _desc) : CError() { *this << _hr << _desc; }
CError::~CError (void) {}

/////////////////////////////////////////////////////////////////////////////

LPCWSTR  CError::Desc  (void) const { return m_desc.GetString(); }
bool     CError::Is    (void) const { return !!FAILED(m_result); }
HRESULT  CError::Result(void) const { return m_result; }

/////////////////////////////////////////////////////////////////////////////

CError&  CError::operator = (const CError& _ref ) { *this << _ref.Desc() << _ref.Result(); return *this; }
CError&  CError::operator <<(const HRESULT _hr  ) { this->m_result = _hr; return *this; }
CError&  CError::operator <<(const LPCWSTR _desc) { this->m_desc = _desc; return *this; }

/////////////////////////////////////////////////////////////////////////////

CError::operator bool    (void) const { return this->Is(); }
CError::operator HRESULT (void) const { return this->Result(); }
CError::operator LPCWSTR (void) const { return this->Desc(); }

/////////////////////////////////////////////////////////////////////////////

CDigit_Sum:: CDigit_Sum (void) : m_predict(0) {}
CDigit_Sum:: CDigit_Sum (const CDigit_Sum& _ref) : CDigit_Sum() { *this = _ref; }
CDigit_Sum:: CDigit_Sum (const UINT u_pred) : CDigit_Sum() { *this << u_pred; }
CDigit_Sum::~CDigit_Sum (void) {}

/////////////////////////////////////////////////////////////////////////////

LONG    CDigit_Sum::Max (const LONG _value) const {
	_value;
	if (0 == this->Predict())
		return _value;

	const LONG n_max = wanderer::_impl::Get_Max(CNumber_U(_value).CapacityE(),  this->Predict(), _value);
	return n_max;
}

LONG    CDigit_Sum::Min (const LONG _value) const {
	_value;
	if (0 == this->Predict())
		return _value;

	const LONG n_min = wanderer::_impl::Get_Min(CNumber_U(_value).CapacityE(),  this->Predict(), _value);
	return n_min;
}

CNode_U CDigit_Sum::Max (const CNode_U& _node) const { CNode_U u_result;
	_node;
	if (this->Is() == false) {
		return (u_result = _node); // predict is not set; return result as input one;
	}
	if (this->Predict() < *this >> _node) {
		return (u_result = _node); // predict is less than _node's one: cannot move forward; return result as input one;
	}
	const SIZE sz_sum_ = { // initial values of the input _node; it requires for getting a distribution of digit sum among {x:y} coordinates;
		static_cast<LONG>(*this >> _node.X()),
		static_cast<LONG>(*this >> _node.Y())
	};

	u_result.X() = wanderer::_impl::Get_Max(CNumber_U(_node.X()).CapacityE(), this->Predict() - sz_sum_.cy, _node.X());
	u_result.Y() = wanderer::_impl::Get_Max(CNumber_U(_node.Y()).CapacityE(), this->Predict() - sz_sum_.cx, _node.Y());
	
	return u_result;
}

CNode_U CDigit_Sum::Min (const CNode_U& _node) const { CNode_U u_result;
	_node;
	if (this->Is() == false) {
		return (u_result = _node); // predict is not set; return result as input one;
	}
	if (this->Predict() < *this >> _node) {
		return (u_result = _node); // predict is less than _node's one: cannot move backward; return result as input one;
	}
	const SIZE sz_sum_ = { // initial values of the input _node; it requires for getting a distribution of digit sum among {x:y} coordinates;
		static_cast<LONG>(*this >> _node.X()),
		static_cast<LONG>(*this >> _node.Y())
	};

	u_result.X() = wanderer::_impl::Get_Min(CNumber_U(_node.X()).CapacityE(), this->Predict() - sz_sum_.cy, _node.X());
	u_result.Y() = wanderer::_impl::Get_Min(CNumber_U(_node.Y()).CapacityE(), this->Predict() - sz_sum_.cx, _node.Y());
	
	return u_result;
}

/////////////////////////////////////////////////////////////////////////////
TWErrorRef
       CDigit_Sum::Error   (void) const { return m_error   ; }
LONG   CDigit_Sum::Get     (const LONG _x, const LONG _y) const {
	return (*this << _x) + (*this << _y);
}
bool   CDigit_Sum::Is      (void) const {
	if (0==m_predict)
	     m_error << E_NOT_SET << _T("Limit of digit sum is not set;");
	else m_error << S_OK << _T("#no_error"); return m_predict > 0;
}
UINT   CDigit_Sum::Predict (void) const { return m_predict ; }
UINT&  CDigit_Sum::Predict (void)       { return m_predict ; }

/////////////////////////////////////////////////////////////////////////////
const  // https://stackoverflow.com/questions/37802501/sum-of-digits-program-not-giving-correct-answer-for-negative-number-in-c
LONG   CDigit_Sum::operator << (const LONG _number) const {
	_number;
	LONG number_ = _number; LONG sum_ =  0; while (number_) {
		INT re_ = number_ % 10;
		number_ = number_ / 10;
		sum_   += number_ ? abs(re_) : re_;
	}
	return  sum_;
}
const
UINT   CDigit_Sum::operator >> (const UINT _number) const {
	_number;
	UINT number_ = _number; UINT sum_ =  0; for (sum_ = 0; number_ > 0; sum_ += number_ % 10, number_ /= 10);
	return sum_;
}
const
UINT   CDigit_Sum::operator >> (const CNode_U& _node) const { return (*this >> _node.X()) + (*this >> _node.Y()); }

/////////////////////////////////////////////////////////////////////////////

CDigit_Sum&  CDigit_Sum::operator = (const CDigit_Sum& _ref) { this->Predict() = _ref.Predict(); this->m_error = _ref.Error(); return *this; }

/////////////////////////////////////////////////////////////////////////////

CSurface:: CSurface (void) {}
CSurface:: CSurface (const CSurface& _ref) : CSurface() { *this = _ref; }
CSurface::~CSurface (void) {}

/////////////////////////////////////////////////////////////////////////////

const CNode_U& CSurface::Entry (void) const { return m_entry; }
const CError&  CSurface::Error (void) const { return m_error; }
const CRange&  CSurface::Horz  (void) const { return m_horz ; }
const CRange&  CSurface::Vert  (void) const { return m_vert ; }

/////////////////////////////////////////////////////////////////////////////

HRESULT CSurface::Calc_Extrema ( const CNode_U& _entry_pt, const UINT _sum) {
	_entry_pt; _sum;
	return this->Calc_Extrema(_entry_pt.X(), _entry_pt.Y(), _sum);
}

HRESULT CSurface::Calc_Extrema ( const LONG _x_coord, const LONG _y_coord, const UINT _sum) {
	_x_coord; _y_coord;
	m_error << S_OK << __FUNCTIONW__;
	if (0 == _sum)
		return ((m_error << E_INVALIDARG) << _T("Digit sum is invalid;"));

	CDigit_Sum digi_sum; digi_sum.Predict() = _sum;

	CNode_U max_coords = digi_sum.Max(CNode_U(_x_coord, _y_coord));
	CNode_U min_coords = digi_sum.Min(CNode_U(_x_coord, _y_coord));
	// TODO: negative values must be taking into account; neither is checked nor tested yet;
	m_horz.Max() = CNode_U(max_coords.X(), _y_coord); m_horz.Min() = CNode_U(min_coords.X(), _y_coord);
	m_vert.Max() = CNode_U(_x_coord, max_coords.Y()); m_vert.Min() = CNode_U(_x_coord, min_coords.Y());

	m_entry.X () = _x_coord;
	m_entry.Y () = _y_coord;

	return m_error ;
}

/////////////////////////////////////////////////////////////////////////////

CSurface&  CSurface::operator = (const CSurface& _ref) {
	this->m_horz = _ref.m_horz; this->m_vert = _ref.m_vert; this->m_error = _ref.Error(); this->m_entry = _ref.Entry(); return *this;
}

/////////////////////////////////////////////////////////////////////////////

CAcc_Space:: CAcc_Space (void) : m_predict(0) { ::RtlZeroMemory(m_secs, sizeof(m_secs)); }
CAcc_Space:: CAcc_Space (const CAcc_Space& _ref) : CAcc_Space() { *this = _ref; }
CAcc_Space::~CAcc_Space (void) {}

/////////////////////////////////////////////////////////////////////////////
#if defined(_succeess)
ULONG CAcc_Space::Area (void) { ULONG u_area = 0;
	m_error << S_OK << __FUNCTIONW__;

	const bool b_y_inc = TBase::Horz().Include(TBase::Entry());
	const bool b_x_inc = TBase::Vert().Include(TBase::Entry());
	static const INT n_shift = 1;

	::RtlZeroMemory(m_secs, sizeof(m_secs));

	if (b_y_inc && b_x_inc) {

		SIZE sz_delta = {0};

		// (1) left-top section/region of serfice; TODO: not tested yet; signed value operation may lead to tricks;
		sz_delta.cx = (
			TBase::Entry().X() > TBase::Horz().Min().X() ? TBase::Entry().X() - TBase::Horz().Min().X() : TBase::Horz().Min().X() - TBase::Entry().X()
		);
		sz_delta.cy = (
			TBase::Vert().Max().Y() > TBase::Entry().Y() ? TBase::Vert().Max().Y() - TBase::Entry().Y() : TBase::Entry().Y() - TBase::Vert().Max().Y()
		);

		if (sz_delta.cx && sz_delta.cy) {
			sz_delta.cy += n_shift;
			for (LONG i_ = TBase::Horz().Min().X(); i_ < TBase::Entry().X() + n_shift && -1 < sz_delta.cy ; i_++) {
				m_secs[_secs::e_left_top] += sz_delta.cy; sz_delta.cy -= 1;
			}
		}

		// (2) left-bottom section/region of serfice; TODO: not tested yet;

		sz_delta.cx = (
			TBase::Entry().X() > TBase::Horz().Min().X() ? TBase::Entry().X() - TBase::Horz().Min().X() : TBase::Horz().Min().X() - TBase::Entry().X()
		);
		sz_delta.cy = (
			TBase::Entry().Y() > TBase::Vert().Min().Y() ? TBase::Entry().Y() - TBase::Vert().Min().Y() : TBase::Vert().Min().Y() - TBase::Entry().Y()
		);

		if (sz_delta.cx && sz_delta.cy) {
			sz_delta.cy += n_shift;
			for (LONG i_ = TBase::Horz().Min().X(); i_ < TBase::Entry().X() + n_shift && -1 < sz_delta.cy ; i_++) {
				m_secs[_secs::e_left_bottom] += sz_delta.cy; sz_delta.cy -= 1;
			}
		}

		// (3) right-top section/region of the surface;
		sz_delta.cx = (TBase::Horz().Max().X() - TBase::Entry().X());
		sz_delta.cy = (TBase::Vert().Max().Y() - TBase::Entry().Y());

		if (sz_delta.cx && sz_delta.cy) {
			sz_delta.cy += n_shift;
			for (LONG i_ = this->Entry().X(); i_ < TBase::Horz().Max().X() + n_shift; i_++) {
				m_secs[_secs::e_right_top] += sz_delta.cy; sz_delta.cy -= 1;
			}
		}

		// (4) right-bottom section/region of the surface; TODO: not tested yet;
		sz_delta.cx = (TBase::Horz().Max().X() - TBase::Entry().X());
		sz_delta.cy = (
			TBase::Entry().Y() > TBase::Vert().Min().Y() ? TBase::Entry().Y() - TBase::Vert().Min().Y() : TBase::Vert().Min().Y() - TBase::Entry().Y()
		);

		if (sz_delta.cx && sz_delta.cy) {
			sz_delta.cy += n_shift;
			for (LONG i_ = this->Entry().X(); i_ < TBase::Horz().Max().X() + n_shift; i_++) {
				m_secs[_secs::e_right_bottom] += sz_delta.cy; sz_delta.cy -= 1;
			}
		}

		u_area = m_secs[_secs::e_left_top] + m_secs[_secs::e_left_bottom] + m_secs[_secs::e_right_top] + m_secs[_secs::e_right_bottom];
	}


	if (0 == u_area) {  // stupid error; must be removed or re-designed;
		m_error << __DwordToHresult(ERROR_INVALID_DATA) << _T("It looks like neither surface is calculated nor entry point is set;");
		u_area = 1;     // available area equals to entry point anyway;
	}

	return u_area;
}
#else
ULONG     CAcc_Space::Area (void) { ULONG u_area = 0; m_error << S_OK << __FUNCTIONW__;

	::RtlZeroMemory(m_secs, sizeof(m_secs));

	const bool b_y_inc = TBase::Horz().Include(TBase::Entry());
	const bool b_x_inc = TBase::Vert().Include(TBase::Entry());

	if (!b_y_inc || !b_x_inc)
		return 0;
	// make assumption reagarding of acceptable cells in to-right sector; that means: x >> increasing to right and y >> increasing to top;

	// (1) moves from left to right by x-axis (toward the right); y-coord value is increased for each x-coord;
	for (LONG x_ = TBase::Entry().X(); x_ <= TBase::Horz().Max().X(); x_++) {
		this->To_Top(x_, TBase::Entry().Y());
	}
	// (2) moves from bottom to top by y-axis (toward the top); x-coord value is increased for each y-coord;
	for (LONG y_ = TBase::Entry().Y(); y_ <= TBase::Vert().Max().Y(); y_++) {
		this->To_Right(TBase::Entry().X(), y_);
	}
	return (u_area = static_cast<ULONG>(m_cache.size()));
}
#endif
/////////////////////////////////////////////////////////////////////////////
const ::std::vector<CNode_U>&
          CAcc_Space::Cache (void) const { return m_cache; }
bool      CAcc_Space::Has   (const CNode_U& _node) const {
	return this->Has(_node.X(), _node.Y());
}
bool      CAcc_Space::Has   (const LONG _x, const LONG _y) const { bool b_has = false;
	_x; _y;
	if (m_cache.empty())
		return (b_has = false);
	// very stupid approach, but it is just for test;
	for (size_t i_ = 0; i_ < m_cache.size(); i_++) {
		if (m_cache[i_].X() != _x) continue;
		if (m_cache[i_].Y() != _y) continue;
		b_has = true; break;
	}
	
	return b_has;
}

UINT      CAcc_Space::Predict (void) const { return m_predict; }
UINT&     CAcc_Space::Predict (void)       { return m_predict; }
#if defined(TRUE)
CStringW  CAcc_Space::Print (void) const { CStringW cs_out;
	cs_out.Format(
		_T("sectors: l-t=%d; l-b=%d; r-t=%d; r-b=%d; area=%d"),
		m_secs[_secs::e_left_top], m_secs[_secs::e_left_bottom], m_secs[_secs::e_right_top], m_secs[_secs::e_right_bottom],
		m_secs[_secs::e_left_top]+ m_secs[_secs::e_left_bottom]+ m_secs[_secs::e_right_top]+ m_secs[_secs::e_right_bottom]
	);
	return cs_out;
}
#endif
void      CAcc_Space::Reset (void) {
	if (m_cache.empty() == false) m_cache.clear(); m_predict = 0;
}
/////////////////////////////////////////////////////////////////////////////

bool  CAcc_Space::To_Bottom(const CNode_U& _node) { return this->To_Bottom(_node.X(), _node.Y()); }
bool  CAcc_Space::To_Bottom(const LONG _x, const LONG _y) { bool b_result = false;
	_x; _y;
	if (0 == this->Predict()) return b_result; // no beforehand check for this time;

	LONG y_ = _y;

	while (CDigit_Sum().Get(_x, y_) <= static_cast<LONG>(this->Predict())) {
		if (this->Has(_x, y_) == false)
			try { m_cache.push_back(CNode_U(_x,y_)); }
				catch (const ::std::bad_alloc&) { break; }
		b_result = true;
		y_--; // decreasing y-coordinate value;
	}

	return b_result;
}

bool  CAcc_Space::To_Left  (const CNode_U& _node) { return this->To_Left(_node.X(), _node.Y()); }
bool  CAcc_Space::To_Left  (const LONG _x, const LONG _y) { bool b_result = false;
	_x; _y;
	if (0 == this->Predict()) return b_result; // no beforehand check for this time;

	LONG x_ = _x;

	while (CDigit_Sum().Get(x_, _y) <= static_cast<LONG>(this->Predict())) {
		if (this->Has(x_, _y) == false)
			try { m_cache.push_back(CNode_U(x_,_y)); }
		catch (const ::std::bad_alloc&) { break; }
		b_result = true;
		x_--; // decreasing x-coordinate value;
	}

	return b_result;
}

bool  CAcc_Space::To_Right (const CNode_U& _node) { return this->To_Right(_node.X(), _node.Y()); }
bool  CAcc_Space::To_Right (const LONG _x, const LONG _y) { bool b_result = false;
	_x; _y;
	if (0 == this->Predict()) return b_result; // no beforehand check for this time;

	LONG x_ = _x;

	while (CDigit_Sum().Get(x_, _y) <= static_cast<LONG>(this->Predict())) {
		if (this->Has(x_, _y) == false)
			try { m_cache.push_back(CNode_U(x_,_y)); }
		catch (const ::std::bad_alloc&) { break; }
		b_result = true;
		x_++; // increasing x-coordinate value;
	}

	return b_result;
}

bool  CAcc_Space::To_Top   (const CNode_U& _node) { return this->To_Top(_node.X(), _node.Y()); }
bool  CAcc_Space::To_Top   (const LONG _x, const LONG _y) { bool b_result = false;
	_x; _y;
	if (0 == this->Predict()) return b_result; // no beforehand check for this time;

	LONG y_ = _y;

	while (CDigit_Sum().Get(_x, y_) <= static_cast<LONG>(this->Predict())) {
		if (this->Has(_x, y_) == false)
			try { m_cache.push_back(CNode_U(_x,y_)); }
		catch (const ::std::bad_alloc&) { break; }
		b_result = true;
		y_++; // increasing y-coordinate value;
	}

	return b_result;
}

/////////////////////////////////////////////////////////////////////////////

CAcc_Space&  CAcc_Space::operator = (const CAcc_Space& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }