#ifndef _STROLLER_DATA_H_2B104C11_F145_4B0E_98EA_914FDA505C9E_INCLUDED
#define _STROLLER_DATA_H_2B104C11_F145_4B0E_98EA_914FDA505C9E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jan-2021 at 10:02:16.670 pm, UTC+7, Novosibirsk, Friday;
	This is generic stroller-tramp-voyager data interface declaration file.
*/
#include <cmath>

namespace wanderer {

	// TODO: the most of operators must be re-viewed/re-designed or deprecated;

	// https://docs.microsoft.com/en-us/windows/win32/winprog/windows-data-types
	// it looks like there is no difference between INT and LONG under MS Windows platform: both of them is 32-bit data type;
	// thus, these data types are mixed herein, maybe it is not good, but at least for sake of the above note;

	class CNumber_U {
	private:
		UINT  m_value;

	public:
		 CNumber_U (void);
	explicit CNumber_U (const CNumber_U& );
	explicit CNumber_U (const UINT _value);
		~CNumber_U (void);

	public:
		BYTE  Capacity (void) const; // returns number of digits in given value;
		BYTE  CapacityE(void) const;
		UINT  Value    (void) const;
		UINT& Value    (void)      ;
	public:
		operator UINT  (void) const;

	public:
		CNumber_U& operator = (const CNumber_U& );
		CNumber_U& operator <<(const UINT _value);
	};
	
	class CNode_U { // redesigned to signed data type;
	private:
		LONG  m_x;
		LONG  m_y;

	public:
		 CNode_U (void);
		 CNode_U (const CNode_U&);
		 CNode_U (const LONG _x, const LONG _y);
		~CNode_U (void);

	public:
		LONG  X (void) const; // x-coordinate; (get);
		LONG& X (void)      ; // x-coordinate; (get/set);
		LONG  Y (void) const; // y-coordinate; (get);
		LONG& Y (void)      ; // y-coordinate; (get/set);
		LONG  Z (void) const; // z-value; for this class, it's treated as weight of the node, more precisely: this is a value of digit sum;
#if defined(_DEBUG)
		CStringW Print (void) const;
#endif

	public:
		CNode_U& operator = (const CNode_U&);
	public:
		const bool operator < (const CNode_U&) const;
		const bool operator > (const CNode_U&) const;
		const bool operator ==(const CNode_U&) const;
		const bool operator !=(const CNode_U&) const;
	};
#if (0)
	bool operator == (const CNode_U& _lh, const CNode_U& _rh);
	bool operator != (const CNode_U& _lh, const CNode_U& _rh);
	bool operator <= (const CNode_U& _lh, const CNode_U& _rh);
	bool operator >= (const CNode_U& _lh, const CNode_U& _rh);
	bool operator >  (const CNode_U& _lh, const CNode_U& _rh);
	bool operator <  (const CNode_U& _lh, const CNode_U& _rh);
#endif
	class CRange { // [m_min...m_max]
	private:
		CNode_U   m_min;
		CNode_U   m_max;

	public:
		 CRange (void);
		 CRange (const CRange&);
		 CRange (const CNode_U& _min, const CNode_U& _max);
		~CRange (void);

	public:
		bool      Is  (void) const; // returns true if { min < max }; is stupid and is not used yet;
		const
		CNode_U&  Max (void) const;
		CNode_U&  Max (void)      ;
		const
		CNode_U&  Min (void) const;
		CNode_U&  Min (void)      ;
#if defined(_DEBUG)
		CStringW  Print (void) const;
#endif
	public:
		bool  Include (const CNode_U&) const; // checks an inclusion of object provided: {x_min <= x_in <= x_max && y_min <= y_in <= y_max};

	public:
		CRange& operator = (const CRange&);
	};

}

#endif/*_STROLLER_DATA_H_2B104C11_F145_4B0E_98EA_914FDA505C9E_INCLUDED*/