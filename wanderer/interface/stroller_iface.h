#ifndef _STROLLER_IFACE_H_3B9D6D2A_9685_417F_94CC_0860467AC99A_INCLUDED
#define _STROLLER_IFACE_H_3B9D6D2A_9685_417F_94CC_0860467AC99A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jan-2021 at 7:37:57.036 pm, UTC+7, Novosibirsk, Tuesday;
	This is generic stroller-tramp-voyager path discovering interface declaration file.
*/
#include "stroller_data.h"

namespace wanderer {

	class CError {
	private:
		HRESULT   m_result;
		CStringW  m_desc  ;

	public:
		 CError (void);
		 CError (const CError&);
		 CError (const HRESULT, LPCWSTR);
		~CError (void);

	public:
		LPCWSTR  Desc   (void) const;
		bool     Is     (void) const;
		HRESULT  Result (void) const;

	public:
		CError&  operator = (const CError&);
		CError&  operator <<(const HRESULT);
		CError&  operator <<(LPCWSTR);

	public:
		operator bool   (void) const;
		operator LPCWSTR(void) const;
		operator HRESULT(void) const;
	};
}

typedef       wanderer::CError        TWError   ;
typedef const wanderer::CError&       TWErrorRef;

namespace wanderer {


	// https://en.wikipedia.org/wiki/Digit_sum
	//
	// note: getting number of digits of a value encounters using such operations as modulus and division;
	//       maybe it is easier to convert a number to string and to get a length of that string; a performance is not checked, of course;
	//       the above approach can be applied to getting digits' sum: each symbol of digit is converted to integer value and is accumulated;
	//

	class CDigit_Sum {
	private: mutable
		CError m_error  ;
		UINT   m_predict; // digit sum that is required for calculating min and max acceptable values for a number provide; see below;

	public:
		 CDigit_Sum (void) ;
		 CDigit_Sum (const CDigit_Sum&);
		 CDigit_Sum (const UINT u_pred);
		~CDigit_Sum (void) ;

	public:
		LONG   Max (const LONG _value) const; // it is supposed the predict is assigned; returns max value that is acceptible for provided one;
		LONG   Min (const LONG _value) const; // it is supposed the predict is assigned; returns min value that is acceptible for provided one;
		                                      // TODO: ::Min() implementation is dumb and is not optimized; slow performance is very possible;
		CNode_U  Max (const CNode_U&) const;
		CNode_U  Min (const CNode_U&) const;

	public: 
		const CError&
		       Error   (void) const;
		LONG   Get     (const LONG _x, const LONG _y) const;  // gets a value of accumulating digit sum of (x) and digit sum of (y);
		bool   Is      (void) const;          // returns true in case when a predict is not set to zero;
		UINT   Predict (void) const;
		UINT&  Predict (void)      ;

	public:
		const  LONG  operator << (const LONG _number) const;  // returns digit sum value of given number; negative numbers are also acceptable;
		const  UINT  operator >> (const UINT _number) const;  // returns digit sum value of given unsigned number;
		const  UINT  operator >> (const CNode_U&) const;      // returns digit sum of node coordinates;

	public:
		CDigit_Sum&  operator  = (const CDigit_Sum&);
	};

	// https://en.wikipedia.org/wiki/Maxima_and_minima

	class CSurface {
	protected:
		mutable
		CError  m_error;
		CRange  m_horz ; // maximum range of x-coordinates that is accessible for movement;
		CRange  m_vert ; // maximum range of y-coordinates that is accessible for movement;
		CNode_U m_entry; // entry point or coordinates that is input data for calculating accessible area;

	public:
		 CSurface (void);
		 CSurface (const CSurface&);
		~CSurface (void);

	public:
		const CNode_U& Entry (void) const;
		const CError&  Error (void) const;  
		const CRange&  Horz  (void) const; // x-coordinate extremum range;
		const CRange&  Vert  (void) const; // y-coordinate extremum range;

	public:
		HRESULT  Calc_Extrema  ( const CNode_U& _entry_pt, const UINT _sum);  // calculates accessible area for entry point and Z-value (digit sum limit);
		HRESULT  Calc_Extrema  ( const LONG _x_coord, const LONG _y_coord, const UINT _sum);

	public:
		CSurface& operator = (const CSurface&);
	};

	// acc(essible) space;
	class CAcc_Space : public CSurface { typedef CSurface TBase;
	public:
		enum _secs { e_left_top = 0, e_left_bottom = 1, e_right_top = 2, e_right_bottom = 3, e__count };
	private:
		ULONG  m_secs[e__count];         // sectors of the acceptable area: {0-left-top; 1-left-bottom; 2-right-top; 3-right-bottom};
		::std::vector<CNode_U>  m_cache; // available cells;
		UINT   m_predict;                // limit of digital sum value;

	public:
		 CAcc_Space (void);
		 CAcc_Space (const CAcc_Space&);
		~CAcc_Space (void);

	public:
#if defined(_success)
		ULONG     Area  (void);     // gets an area of accessible surface;
#else
		ULONG     Area  (void);
#endif
		const ::std::vector<CNode_U>&
		          Cache (void) const;
		bool      Has   (const CNode_U&) const; // searches the cache;
		bool      Has   (const LONG _x, const LONG _y) const; // searches the cache;
		UINT      Predict (void) const;
		UINT&     Predict (void)      ;
#if defined(TRUE)
		CStringW  Print (void) const;
#endif
		void      Reset (void) ;
	private:      // returning boolean result is useless;
		bool      To_Bottom(const CNode_U&); bool  To_Bottom(const LONG _x, const LONG _y); // means y-coordinate is decreased only;
		bool      To_Left  (const CNode_U&); bool  To_Left  (const LONG _x, const LONG _y); // means x-coordinate is decreased only;
		bool      To_Right (const CNode_U&); bool  To_Right (const LONG _x, const LONG _y); // means x-coordinate is increased only;
		bool      To_Top   (const CNode_U&); bool  To_Top   (const LONG _x, const LONG _y); // means y-coordinate is increased only;
	public:
		CAcc_Space& operator = (const CAcc_Space&);
	};

}
typedef       wanderer::CSurface      TSurface  ;
typedef       wanderer::CAcc_Space    TAvailArea;

#endif/*_STROLLER_IFACE_H_3B9D6D2A_9685_417F_94CC_0860467AC99A_INCLUDED*/