/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Sep-2020 at 6:58:28p, UTC+7, Novosibirsk, Thursday;
	This is FakeGPS driver version 2 desktop client console interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Wanderer Test project on 6-Jan-2021 at 11:46:09.018 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "the_console.h"
#include "the_console.res.h"

using namespace wanderer::test::client;

namespace wanderer { namespace test { namespace client { namespace _impl {

	class CIconLoader {
	public:
		HICON Do  (const UINT nIconResId, const bool bTreatAsLargeIcon)
		{
			const SIZE szIcon = this->Size(bTreatAsLargeIcon);
			const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
			const HICON hIcon = (HICON)::LoadImage(
				hInstance, MAKEINTRESOURCE(nIconResId), IMAGE_ICON, szIcon.cx, szIcon.cy, LR_DEFAULTCOLOR
			);
			return hIcon;
		}
	private:
		SIZE  Size(const bool bTreatAsLargeIcon)
		{
			const SIZE szIcon = {
				::GetSystemMetrics(bTreatAsLargeIcon ? SM_CXICON : SM_CXSMICON), 
				::GetSystemMetrics(bTreatAsLargeIcon ? SM_CYICON : SM_CYSMICON)
			};
			return szIcon;
		}
	};

	class CTimestamp {
	public:
		 CTimestamp (void) {}
		~CTimestamp (void) {}

	public:
		CStringW  out (void) const {

			static LPCTSTR  lp_sz_tm_pat = _T("%02d/%02d/%02d %02d:%02d:%02d.%03d");
			SYSTEMTIME st = { 0 };
			::GetLocalTime(&st);

			CStringW cs_timestamp;
			cs_timestamp.Format(
				lp_sz_tm_pat,
				st.wMonth   ,
				st.wDay     ,
				st.wYear%100,  // only 2 digits
				st.wHour    ,
				st.wMinute  ,
				st.wSecond  ,
				st.wMilliseconds
			);
			return cs_timestamp;
		}
	};

}}}}

using namespace wanderer::test::client::_impl;

/////////////////////////////////////////////////////////////////////////////

CConsole_simplified:: CConsole_simplified (void) : m_wnd(::GetConsoleWindow()) {
	::SetConsoleOutputCP(CP_UTF8);
	m_wnd.SetIcon(CIconLoader().Do(IDR_WANDERER_CON_ICO, 1 ), 1 );
	m_wnd.SetIcon(CIconLoader().Do(IDR_WANDERER_CON_ICO, 0 ), 0 );
}

CConsole_simplified::~CConsole_simplified (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CConsole_simplified::OnCreate (LPCWSTR _lp_sz_cap, LPCWSTR _lp_sz_title) {
	_lp_sz_cap; _lp_sz_title;
	HRESULT hr_ = S_OK;

	::SetConsoleOutputCP(CP_UTF8); std::wstring w_sep_(77, _T('-'));

	m_wnd.SetWindowTextW(_lp_sz_cap);

	CStringW cs_tit(_lp_sz_title);
	if (cs_tit.IsEmpty())
		cs_tit = _T("Generic Console");

	wprintf(_T("%s\n")  , w_sep_.c_str());
	wprintf(_T("\t%s\n"), cs_tit.GetString());
	wprintf(_T("%s\n")  , w_sep_.c_str());

	return hr_;
}

HRESULT  CConsole_simplified::OnWait   (LPCWSTR _lp_sz_msg) { HRESULT hr_ = S_OK;
	_lp_sz_msg;
	const bool b_verb = true;
	if (b_verb) {
		if (NULL != _lp_sz_msg)
			::wprintf(_T("\n\n\t%s"), _lp_sz_msg);
		else
			::wprintf(_T("\n\n\tPress any key to continue"));
		::_getwch();
	}
	return  hr_;
}

CWindow& CConsole_simplified::Window   (void) { return m_wnd; }

/////////////////////////////////////////////////////////////////////////////

CConsole_simplified& CConsole_simplified::operator <<(LPCWSTR _lp_sz_info) {  // prints out an information;
	_lp_sz_info;
	CStringW cs_out; cs_out.Format(_T("\n\t[INFO][%s] %s"), CTimestamp().out().GetString(), _lp_sz_info);
	::wprintf(_T("%s"), cs_out.GetString());

	return *this;
}

CConsole_simplified& CConsole_simplified::operator >>(LPCWSTR _lp_sz_err) {  // prints out an error details
	_lp_sz_err;
	CStringW cs_out; cs_out.Format(_T("\n\t[#ERR][%s] %s"), CTimestamp().out().GetString(), _lp_sz_err);
	::wprintf(_T("%s"), cs_out.GetString());

	return *this;
}

CConsole_simplified& CConsole_simplified::operator <=(LPCWSTR _lp_sz_warn) {  // prints out an warning;
	_lp_sz_warn;
	CStringW cs_out; cs_out.Format(_T("\n\t[WARN][%s] %s"), CTimestamp().out().GetString(), _lp_sz_warn);
	::wprintf(_T("%s"), cs_out.GetString());

	return *this;
}