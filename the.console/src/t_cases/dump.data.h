#pragma once
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Oct-2020 at 5:56:40a, UTC+7, Novosibirsk, Tuesday;
	This is generic data dumper interface declaration file.
*/
#include <Shlwapi.h>
#pragma comment(lib, "Shlwapi.lib")

#include "stroller_data.h"
#include "stroller_iface.h"

namespace wanderer { namespace test { namespace dump {

	class CAuto_Handle {
	private:
		HANDLE    m_handle;

	public:
		 CAuto_Handle (void);
		 CAuto_Handle (const CAuto_Handle&);
		 CAuto_Handle (HANDLE _handle);
		~CAuto_Handle (void);

	public:
		bool  Is (void) const;

	public:
		operator const HANDLE& (void) const;
		operator       HANDLE& (void)      ;

	public:
		CAuto_Handle& operator = (HANDLE _handle);
	};

	class CFile_Path {
	private:
		WCHAR   m_buffer[_MAX_PATH];

	public:
		 CFile_Path (void);
		 CFile_Path (const CFile_Path&);
		~CFile_Path (void);

	public:
		LPCWSTR  Get (void) const;
		HRESULT  Set (LPCWSTR _lp_sz_file_name);

	public:
		CFile_Path&  operator = (const CFile_Path&);
		CFile_Path&  operator <<(LPCWSTR _lp_sz_file_name);

	public:
		operator LPCWSTR (void) const;
	};

	class CDumper {
	private:
	//	CFile_Path   m_path  ;
		PBYTE        m_buffer;
		DWORD        m_buffer_sz;
		CAuto_Handle m_file ;
		CStringW     m_path ;
		UINT         m_total;   // total number of cells reviewed;
		UINT         m_errors;  // number of cells that do not confirm to required digit sum;

	public:
		 CDumper (void);
		~CDumper (void);

	public:
		const
		LPBYTE       Data  (void) const;
		bool         Is    (void) const;
	//	const
	//	CFile_Path&  Path  (void) const;
	//	CFile_Path&  Path  (void)      ;
		LPCWSTR      Path  (void) const;
		HRESULT      Path  (LPCWSTR)   ;
		HRESULT      Reset (void)      ;
		DWORD        Size  (void) const;

	public:
		UINT         Errors(void) const;
		UINT         Total (void) const;
	public:
		HRESULT      To_Csv(const wanderer::CAcc_Space&); // https://en.wikipedia.org/wiki/Comma-separated_values

	public:
		HRESULT      Append(const LPBYTE _p_data, const DWORD _d_data_sz);
		HRESULT      Errors(const wanderer::CAcc_Space&, const UINT _sum);       // looks for cells that have digit sum greater than specified;
		HRESULT      Load  (void)      ;
		HRESULT      Save  (const LPBYTE _p_data, const DWORD _d_data_sz) const; // data is not cached in the internal buffer;

	public:
		CDumper&     operator << (const wanderer::CAcc_Space&);
		CDumper&     operator << (const wanderer::CRange&);
	};

}}}

typedef wanderer::test::dump::CFile_Path  TFilePath;
typedef wanderer::test::dump::CDumper     TDumper;