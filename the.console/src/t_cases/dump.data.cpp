/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Oct-2020 at 5:56:56a, UTC+7, Novosibirsk, Tuesday;
	This is generic data dumper interface implementation file.
*/
#include "StdAfx.h"
#include "dump.data.h"

using namespace wanderer::test::dump;

/////////////////////////////////////////////////////////////////////////////

CAuto_Handle:: CAuto_Handle (void) : m_handle(INVALID_HANDLE_VALUE) {}
CAuto_Handle:: CAuto_Handle (const CAuto_Handle& _ref) : CAuto_Handle() {
	CAuto_Handle& ref_ = const_cast<CAuto_Handle&>(_ref); *this = _ref; ref_ = (HANDLE)INVALID_HANDLE_VALUE;
}
CAuto_Handle:: CAuto_Handle (HANDLE _handle) : CAuto_Handle() { *this = _handle; }
CAuto_Handle::~CAuto_Handle (void) { if (this->Is()) {::CloseHandle(m_handle); m_handle = NULL; } }

/////////////////////////////////////////////////////////////////////////////

bool  CAuto_Handle::Is (void) const { return (INVALID_HANDLE_VALUE != m_handle && NULL != m_handle); }

/////////////////////////////////////////////////////////////////////////////

CAuto_Handle::operator const HANDLE& (void) const { return m_handle; }
CAuto_Handle::operator       HANDLE& (void)       { return m_handle; }

/////////////////////////////////////////////////////////////////////////////

CAuto_Handle& CAuto_Handle::operator = (HANDLE _handle) { if (this->Is()) {::CloseHandle(m_handle); } m_handle = _handle; return *this; }

/////////////////////////////////////////////////////////////////////////////

CFile_Path:: CFile_Path (void) { ::memset(&m_buffer, 0, sizeof(m_buffer)); }
CFile_Path:: CFile_Path (const CFile_Path& _ref) : CFile_Path() { *this = _ref; }
CFile_Path::~CFile_Path (void) {}

/////////////////////////////////////////////////////////////////////////////

LPCWSTR  CFile_Path::Get (void) const { return m_buffer; }
HRESULT  CFile_Path::Set (LPCWSTR _lp_sz_file_name) { HRESULT hr_ = S_OK;

	if (NULL == _lp_sz_file_name || 0 == ::lstrlenW(_lp_sz_file_name))
		return (hr_ = E_INVALIDARG);

	::memset(&m_buffer, 0, sizeof(m_buffer));

	WCHAR buffer[_MAX_PATH] = {0};
#if (0)
	// https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getmodulefilenamew
	DWORD d_copied = ::GetModuleFileNameW(NULL, buffer, _MAX_PATH - 1);
	if (0 == d_copied)
		return (hr_ = __LastErrToHresult());

	_trace_ex_w(_T("fk_gps_v2: [INFO] %s::path_len: %d;"), __FUNCTIONW__, d_copied);
#else
	DWORD d_copied = ::GetTempPathW(_MAX_PATH - 1, buffer);
	if (0 == d_copied)
		return (hr_ = __LastErrToHresult());
#endif

	LPCWCHAR lp_found = ::wcsrchr(buffer, _T('\\'));
	if (lp_found) {
		::wcsncpy_s(m_buffer, _MAX_PATH - 1, buffer, lp_found - buffer + 1);
		::wcscat_s (m_buffer, _MAX_PATH - 1, _lp_sz_file_name);
	}
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CFile_Path&  CFile_Path::operator = (const CFile_Path& _ref) { ::memcpy_s(&m_buffer, _MAX_PATH, _ref.m_buffer, _MAX_PATH); return *this; }
CFile_Path&  CFile_Path::operator <<(LPCWSTR _lp_sz_file_name) { this->Set(_lp_sz_file_name); return *this; }

/////////////////////////////////////////////////////////////////////////////

CFile_Path::operator LPCWSTR (void) const { return this->Get(); }

/////////////////////////////////////////////////////////////////////////////

CDumper:: CDumper (void) : m_buffer(NULL), m_buffer_sz(0), m_total(0), m_errors(0) {}
CDumper::~CDumper (void) { if (this->Is()) this->Reset(); }

/////////////////////////////////////////////////////////////////////////////
const
LPBYTE   CDumper::Data  (void) const { return  m_buffer; }
bool     CDumper::Is    (void) const { return (m_buffer != NULL); }
#if (0)
const
CFile_Path& CDumper::Path (void) const { return m_path; }
CFile_Path& CDumper::Path (void)       { return m_path; }
#else
LPCWSTR  CDumper::Path  (void) const { return m_path.GetString(); }
HRESULT  CDumper::Path  (LPCWSTR _path) { m_path = _path; return S_OK; }
#endif
HRESULT  CDumper::Reset (void) { HRESULT hr_ = S_OK;

	if (this->Is() == false)
		return (hr_ = OLE_E_BLANK);

	::HeapFree (::GetProcessHeap(), 0, m_buffer); m_buffer = NULL; m_buffer_sz = 0; // no error handling yet;

	return hr_;
}
DWORD    CDumper::Size  (void) const { return m_buffer_sz; }

/////////////////////////////////////////////////////////////////////////////

UINT     CDumper::Errors(void) const { return m_errors; }
UINT     CDumper::Total (void) const { return m_total ; }

/////////////////////////////////////////////////////////////////////////////

HRESULT  CDumper::To_Csv(const wanderer::CAcc_Space& _area) { HRESULT hr_ = S_OK;
	_area;
	const SIZE sz_mtx = {
		_area.Horz().Max().X() - _area.Entry().X() + 1,
		_area.Vert().Max().Y() - _area.Entry().Y() + 1
	};
	// https://en.wikipedia.org/wiki/Matrix_(mathematics)
	// builds a matrix of specified dimensions: sz_mtx.cx is a number of columns; sz_mtx.cy is a number of rows;
	// taking into account an MS excel spreadsheet starts from top-left corner,
	// csv data must follow to direction: top-to-down for rows and left-to-right for columns;

	::std::vector<std::vector<CNode_U>> mtx(sz_mtx.cy);
	for (size_t y_= 0; y_ < sz_mtx.cy; y_++) {
		std::vector<CNode_U>& row_ = mtx[y_]; row_.resize(sz_mtx.cx);
	}

	for (size_t i_ = 0; i_ < _area.Cache().size(); i_++) {
		const CNode_U& node_ = _area.Cache()[i_];

		const POINT pt_anchor = {
			node_.X() - _area.Entry().X(), // column index;
			node_.Y() - _area.Entry().Y()  // row index; 
		};

		if (pt_anchor.y > sz_mtx.cy)
			continue; // out of matrix row count;
		if (pt_anchor.x > sz_mtx.cx)
			continue; // out of matrix column count;

		CNode_U& mtx_element = mtx[pt_anchor.y][pt_anchor.x];
		mtx_element = node_;
	}

	CStringA cs_mtx;

	// puts out the matrix;
	for (size_t y_ = mtx.size(); y_ > 0; --y_) {
		CStringA cs_row;
		const std::vector<CNode_U>& row_ = mtx[y_ - 1];
		for (size_t x_ = 0; x_ < row_.size(); ++x_) {
			const CNode_U& cell_ = row_[x_];
			if (cell_.X() || cell_.Y()) {
				CStringA cs_cell;
				// excel uses [RC] notation, where [R] is row index and [C] is column one;
				cs_cell.Format("%d|%d", cell_.Y(), cell_.X());
				cs_row += cs_cell;
			}
			cs_row += ","; // the last element of a row is not detected yet;
		}
		cs_mtx += cs_row;
		cs_mtx += "\r";
	}
	hr_ =this->Append((LPBYTE)cs_mtx.GetBuffer(), cs_mtx.GetLength() * sizeof(CHAR));

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CDumper::Append(const LPBYTE _p_data, const DWORD _d_data_sz) { HRESULT hr_ = S_OK;
	_p_data; _d_data_sz;
	if (NULL == _p_data) return (hr_ = E_POINTER);
	if ( 0 == _d_data_sz) return (E_INVALIDARG);

	if (m_file.Is() == false) {
		m_file = ::CreateFileW(this->Path(), FILE_APPEND_DATA, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (INVALID_HANDLE_VALUE == m_file) {
			return (hr_ = __LastErrToHresult());
		}
	}

	DWORD d_written = 0;
	const BOOL b_result = ::WriteFile(m_file, _p_data, _d_data_sz, &d_written, NULL);
	if (FALSE == b_result)
		hr_ = __LastErrToHresult();

	return hr_;
}

HRESULT  CDumper::Errors(const wanderer::CAcc_Space& _area, const UINT _sum) { HRESULT hr_ = S_OK;

	const CNode_U en_pt = _area.Entry();
	const CRange& v_rng = _area.Vert ();
	const CRange& h_rng = _area.Horz (); m_errors = m_total = 0;

	CStringW cs_cell;
	CStringW cs_row ;

	// this condition assumes that top-right sector of coordinates is valid;
	if (en_pt.Y() < v_rng.Max().Y() && en_pt.X() < h_rng.Max().X() && en_pt == h_rng.Min() && en_pt == v_rng.Min()) {

		// taking into account data to file is written from first line (top) to the last one (end line; bottom);
		// the data is going to be written from the highest Y coordinate and the initial X coord to down;
		LONG cx = 0;
		for (LONG y_ = v_rng.Max().Y(); y_ >= v_rng.Min().Y() && cx <= (h_rng.Max().X() - h_rng.Min().X()); y_--) {
			for (LONG x_ = 0; x_ <= cx; x_++) {
				const UINT sum_ = (CDigit_Sum() >> (en_pt.X() + x_)) + (CDigit_Sum() >> y_);
				if (sum_ > _sum) {
					cs_cell.Format(
						_T("{%d|%d|%02u};"), en_pt.X() + x_, y_, sum_
					);
					cs_row += cs_cell; m_errors += 1;
				}
				m_total += 1;
			}
			if (cs_row.IsEmpty() == false) {
				cs_row += _T("\n\r");
				this->Append((LPBYTE)cs_row.GetBuffer(), cs_row.GetLength() * sizeof(WCHAR));
				cs_row.Empty();
			}
			cx++;
		}
	}

	return hr_;
}

HRESULT  CDumper::Load  (void) { HRESULT hr_ = S_OK;

	if (this->Is())
		return (hr_ = __DwordToHresult(ERROR_ALREADY_INITIALIZED));

	if (NULL == this->Path())
		return (hr_ = OLE_E_BLANK);

	CAuto_Handle h_file = ::CreateFileW(this->Path(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
	if (h_file.Is() == false) {
		return (hr_ = __LastErrToHresult());
	}

	LARGE_INTEGER req_size = {0};
	BOOL b_result = ::GetFileSizeEx(h_file, &req_size);
	if (FALSE == b_result || 0 == req_size.LowPart)
		return (hr_ = __LastErrToHresult());

	m_buffer = reinterpret_cast<LPBYTE>(::HeapAlloc (::GetProcessHeap(), HEAP_ZERO_MEMORY, req_size.LowPart));
	if (NULL == m_buffer)
		return (hr_ = E_OUTOFMEMORY);

	m_buffer_sz = req_size.LowPart;

	DWORD d_read = 0;
	b_result = ::ReadFile(h_file, m_buffer, m_buffer_sz, &d_read, NULL);
	if (FALSE == b_result)
		return (hr_ = __LastErrToHresult());

	return hr_;
}

HRESULT  CDumper::Save  (const LPBYTE _p_data, const DWORD _d_data_sz) const { HRESULT hr_ = S_OK;
	_p_data; _d_data_sz;

	if (NULL == _p_data)
		return (hr_ = E_POINTER);

	if (0 == _d_data_sz)
		return (hr_ = E_INVALIDARG);

	if (NULL == this->Path())
		return (hr_ = OLE_E_BLANK);

	CAuto_Handle h_file = ::CreateFileW(this->Path(), FILE_APPEND_DATA, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == h_file) {
		return (hr_ = __LastErrToHresult());
	}

	DWORD d_written = 0;
	const BOOL b_result = ::WriteFile(h_file, _p_data, _d_data_sz, &d_written, NULL);
	if (FALSE == b_result)
		hr_ = __LastErrToHresult();

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CDumper& CDumper::operator << (const wanderer::CAcc_Space& _area) {

	const CNode_U en_pt = _area.Entry();
	const CRange& v_rng = _area.Vert ();
	const CRange& h_rng = _area.Horz ();

	CStringW cs_cell;
	CStringW cs_row ;

	// this condition assumes that top-right sector of coordinates is valid;
	if (en_pt.Y() < v_rng.Max().Y() && en_pt.X() < h_rng.Max().X() && en_pt == h_rng.Min() && en_pt == v_rng.Min()) {

	// taking into account data to file is written from first line (top) to the last one (end line; bottom);
	// the data is going to be written from the highest Y coordinate and the initial X coord to down;
		LONG cx = 0;
		for (LONG y_ = v_rng.Max().Y(); y_ >= v_rng.Min().Y() && cx <= (h_rng.Max().X() - h_rng.Min().X()); y_--) {
			for (LONG x_ = 0; x_ <= cx; x_++) {
				cs_cell.Format(
					_T("{%d|%d|%02u};"), en_pt.X() + x_, y_, (CDigit_Sum() >> (en_pt.X() + x_)) + (CDigit_Sum() >> y_)
				);
				cs_row += cs_cell;
			}
			cs_row += _T("\n\r");
			this->Append((LPBYTE)cs_row.GetBuffer(), cs_row.GetLength() * sizeof(WCHAR));
			cs_row.Empty();
			cx++;
		}
	}
	return *this;
}

CDumper& CDumper::operator << (const wanderer::CRange& _range) {

	const CNode_U& min = _range.Min();
	const CNode_U& max = _range.Max();

	CStringW cs_cell;
	CStringW cs_buffer;

	if (min.Y() == max.Y()) { // horizontal range, i.e. this is a row;
		for (LONG i_ = min.X(); i_ <= max.X(); i_++) {
			cs_cell.Format(
				_T("{%d|%d|%u};"), i_, min.Y(), CDigit_Sum() >> (i_ + min.Y())
			);
			cs_buffer += cs_cell;
			if (!(i_ % 10))
				cs_buffer += _T("\r\n");
		}
	}
	else if (min.X() == max.X()) { // vertical range, i.e. this is a column;
		for (LONG i_ = min.Y(); i_ <= max.Y(); i_++) {
			cs_cell.Format(
				_T("{%d|%d|%u};"), min.X(), i_, CDigit_Sum() >> (i_ + min.X())
			);
			cs_buffer += cs_cell;
			if (!(i_ % 10))
				cs_buffer += _T("\r\n");
		}
	}

	this->Append((LPBYTE)cs_buffer.GetBuffer(), cs_buffer.GetLength() * sizeof(WCHAR));

	return *this;
}