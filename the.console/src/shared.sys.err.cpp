/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-May-2012 at 9:44:59pm, GMT+3, Rostov-on-Don, Tuesday;
	This is Pulsepay Server Application Common Error Trace class implementation file;
	-----------------------------------------------------------------------------
	Adopted to File Watcher (thefileguardian.com) on 9-Jul-2018 at 7:56:12p, UTC+7, Phuket, Rawai, Monday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 10:20:24a, UTC+7, Novosibirsk, Friday;
	Adopted to Wanderer Test project on 6-Jan-2021 at 12:23:04.640 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "shared.sys.err.h"

using namespace shared::sys_core;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace sys_core { namespace details {

	LPCWSTR    Error_NotFoundMessage(void) {
		static LPCWSTR pszNotFound = _T("Error details not found");
		return pszNotFound;
	}

	VOID       Error_FormatMessage(const DWORD dwError, ::ATL::CStringW& buffer_ref) {
		WCHAR szBuffer[_MAX_PATH * 2] = {0};

		DWORD d_result = ::FormatMessage(
			FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL    ,
			dwError ,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL), // this is 0x0 flag; otherwise, format does not work;
			szBuffer,
			_MAX_PATH * 2 - 1,
			NULL
		);
		if (d_result == 0) {
			d_result = ::GetLastError(); // for understanding why format does not work;
		}
		buffer_ref = szBuffer;
	}

	VOID       Error_NormalizeMessage(::ATL::CStringW& buffer_ref) {
		if (!buffer_ref.IsEmpty())
		{
			buffer_ref.Replace(_T("\r")  , _T(" "));
			buffer_ref.Replace(_T("\n")  , _T(" "));
			buffer_ref.TrimRight();
		}
	}
}}}
using namespace shared::sys_core::details;
/////////////////////////////////////////////////////////////////////////////

CError:: CError(void) : m_result(OLE_E_BLANK), m_desc(_T("#n/a")) {}
CError::~CError(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD    CError::Code  (void) const     { SAFE_LOCK(m_lock);  return (0xffff & m_result); }
VOID     CError::Code  (const DWORD _v) {
	SAFE_LOCK(m_lock);  m_result = __DwordToHresult(_v);
	Error_FormatMessage(_v, m_desc);
	Error_NormalizeMessage( m_desc);
}
LPCWSTR  CError::Desc  (void) const     { SAFE_LOCK(m_lock);  return m_desc.GetString(); }
CStringW CError::Format(LPCWSTR lp_sz_sep) const {

	static LPCWSTR lp_sz_pat = _T("; Code=0x%x; desc=%s; module=%s;");

	CStringW cs_pat(lp_sz_pat);

	if (lp_sz_sep && ::wcslen(lp_sz_sep))
		cs_pat.Replace(_T("; "), lp_sz_sep);

	CStringW cs_desc;
	cs_desc.Format(
		cs_pat, this->Result(),
		::wcslen(this->Desc())   ? this->Desc()   : _T("#n/a"),
		::wcslen(this->Module()) ? this->Module() : _T("#n/a")
		);
	return cs_desc;
}
LPCWSTR  CError::Module(void) const { SAFE_LOCK(m_lock); return m_module.GetString(); }
VOID     CError::Module(LPCWSTR _v) { SAFE_LOCK(m_lock); m_module = _v;   }
HRESULT  CError::Result(void) const { SAFE_LOCK(m_lock); return m_result; }
VOID     CError::Result(const HRESULT _new) {
	SAFE_LOCK(m_lock);
	m_result = _new;
	if (S_OK == m_result) {
		m_desc = _T("No error");
		return;
	}
	else
	if (m_desc.IsEmpty() == false)
	    m_desc.Empty();

	::ATL::CStringW   cs_module = m_module; // saves an original;
	::ATL::CStringW   cs_source;

	_com_error com_err(_new);
	// https://docs.microsoft.com/en-us/windows/win32/api/oleauto/nf-oleauto-geterrorinfo
	::ATL::CComPtr<IErrorInfo> sp;

	if (S_OK == ::GetErrorInfo(0, &sp)) {

		_com_error err_(m_result, sp , true );
		 com_err = err_;

		 if (com_err.Description().length()) m_desc = (LPCWSTR)com_err.Description();
		 if (com_err.Source().length()) m_module  = (LPCWSTR)com_err.Source();
	}
	else {
		_com_error err_(m_result);
		if (com_err.Description().length()) m_desc = (LPCWSTR)com_err.Description();
		if (com_err.Source().length()) m_module  = (LPCWSTR)com_err.Source();

	}
	if (m_module.IsEmpty())
		m_module = cs_module;
	if (m_desc.IsEmpty())
		details::Error_FormatMessage(this->Code(), m_desc);

	details::Error_NormalizeMessage(m_desc);
}

DWORD      CError::Show  (const HWND _h_owner) const {

	static LPCWSTR lp_sz_pat = _T(
		"Error occurred:"
		"\n  Code   = 0x%x"
		"\n  Desc   = %s"
		"\n  Module = %s"
	);

	CStringW cs_desc;
	cs_desc.Format(
		lp_sz_pat, this->Result(),
		::wcslen( this->Desc  ()) ? this->Desc()   : _T("#n/a"),
		::wcslen( this->Module()) ? this->Module() : _T("#n/a")
	);

	const DWORD d_resp = static_cast<DWORD>(
		::MessageBox(_h_owner, cs_desc.GetString(), _T("Error"), MB_OK | MB_ICONEXCLAMATION)
		);
	return d_resp;
}

VOID     CError::State (const DWORD _code, LPCWSTR _lp_sz_desc) {
	if (_code == ERROR_SUCCESS) *this << S_OK;
	if (_code != ERROR_SUCCESS) *this << HRESULT_FROM_WIN32(_code);
	*this = _lp_sz_desc;
}

VOID     CError::State (HRESULT _err_code, LPCWSTR _lp_err_desc) {
	SAFE_LOCK(m_lock);
	m_result  = _err_code;
	*this = _lp_err_desc;
}

bool     CError::State (void) const { SAFE_LOCK(m_lock); return(TRUE == (FAILED(m_result))); }

/////////////////////////////////////////////////////////////////////////////

CError& CError::operator << (const HRESULT _v) { *this = _v; return *this; }
CError& CError::operator << (LPCWSTR _lp_sz_v) {  this->Module(_lp_sz_v); return *this; }
CError& CError::operator =  (const DWORD   _v) { *this = HRESULT_FROM_WIN32(_v); return *this; }
CError& CError::operator =  (const HRESULT _v) {  this->Result(_v)  ; return *this; }
CError& CError::operator =  (LPCWSTR _lp_sz_v) { m_desc   = _lp_sz_v; return *this; }
CError& CError::operator =  (const CError& _e) {
	SAFE_LOCK(m_lock);
	m_desc   = _e.m_desc;
	m_module = _e.m_module;
	m_result = _e.m_result;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CError::operator const bool (void) const { return this->State(); }
CError::operator HRESULT    (void) const { return this->Result(); }
CError::operator LPCWSTR    (void) const { return this->Desc(); }

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace sys_core {
	//
	// important: operator must return true in cases when error object does not provide success;
	//
	bool operator==(const bool _lhs, const CError& _rhs) { return (_lhs != SUCCEEDED(_rhs.Result())); }
	bool operator!=(const bool _lhs, const CError& _rhs) { return (_lhs == SUCCEEDED(_rhs.Result())); }
	bool operator==(const CError& _lhs, const bool _rhs) { return (SUCCEEDED(_lhs.Result()) != _rhs); }
	bool operator!=(const CError& _lhs, const bool _rhs) { return (SUCCEEDED(_lhs.Result()) == _rhs); }

	bool operator==(const CError& _lhs, const CError& _rhs) { return (_lhs.Code() == _rhs.Code()); }
	bool operator!=(const CError& _lhs, const CError& _rhs) { return (_lhs.Code() != _rhs.Code()); }
}}