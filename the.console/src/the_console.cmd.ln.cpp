/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Feb-2019 at 6:52:05a, UTC+7, Novosibirsk, Sunday;
	This is FIX Engine shared library configuration command line interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Wanderer Test project on 6-Jan-2021 at 11:54:41.852 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "the_console.cmd.ln.h"

using namespace wanderer::test::client;

/////////////////////////////////////////////////////////////////////////////

CCommandLine:: CCommandLine(void)
{
	CStringW cs_cmd_line = ::GetCommandLine();
	CStringW cs_key;
	CStringW cs_arg;

	INT n_count = 0;
	bool bKey   = false;

	LPWSTR* pCmdArgs = ::CommandLineToArgvW(cs_cmd_line.GetString(), &n_count);
	if (0 == n_count || NULL == pCmdArgs)
		goto __end_of_story__;
	
	m_module_full_path = pCmdArgs[0];

	for (INT i_ = 1; i_ < n_count; i_+= 1) {
		CStringW cs_val = pCmdArgs[i_];
		bKey = (/*0 == cs_val.Find(_T("-"))
				|| */0 == cs_val.Find(_T("/")));

		if (bKey) {
			if (cs_key.IsEmpty() == false) // the previous key is not saved yet;
				m_args.insert(::std::make_pair(cs_key, cs_arg));

			cs_key = pCmdArgs[i_]; cs_key.Replace(_T("-"), _T("")); cs_key.Replace(_T("/"), _T(""));
			cs_arg = _T("");
		}
		else {
			cs_arg+= pCmdArgs[i_];
		}

		const bool bLast = (i_ == n_count - 1);
		if (bLast && cs_key.IsEmpty() == false) {
			try {
				m_args.insert(::std::make_pair(cs_key, cs_arg));
			} catch (const ::std::bad_alloc&){}
		}
	}

__end_of_story__:
	if (NULL != pCmdArgs) {
		::LocalFree(pCmdArgs); pCmdArgs = NULL;
	}
}

CCommandLine::~CCommandLine(void) { this->Clear(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CCommandLine::Append(LPCWSTR _lp_sz_nm, LPCWSTR _lp_sz_val) {
	HRESULT hr_ = S_OK;
	try {
		m_args.insert(::std::make_pair(
			CStringW(_lp_sz_nm), CStringW(_lp_sz_val)
		));
	}
	catch(const ::std::bad_alloc&){
		hr_ = E_OUTOFMEMORY;
	}
	return hr_;
}

CStringW      CCommandLine::Arg   (LPCWSTR _lp_sz_nm)const
{
	TCmdLineArgs::const_iterator it_ = m_args.find(CStringW(_lp_sz_nm));
	if (it_ == m_args.end())
		return CStringW();
	else
		return it_->second;
}

LONG          CCommandLine::Arg   (LPCWSTR _lp_sz_nm, const LONG _def_val)const
{
	TCmdLineArgs::const_iterator it_ = m_args.find(CStringW(_lp_sz_nm));
	if (it_ == m_args.end())
		return _def_val;
	else
		return ::_wtol(it_->second);
}

TCmdLineArgs  CCommandLine::Args  (void) const { return m_args; }
VOID          CCommandLine::Clear (void)       { if (m_args.empty() == false) m_args.clear(); }
INT           CCommandLine::Count (void)const  { return static_cast<INT>(m_args.size()); }
bool          CCommandLine::Has   (LPCWSTR pArgName) const
{
	TCmdLineArgs::const_iterator it__ = m_args.find(CStringW(pArgName));
	return (it__ != m_args.end());
}

CStringW      CCommandLine::ModuleFullPath(void) const { return m_module_full_path; }

CStringW      CCommandLine::ToString(LPCWSTR _lp_sz_sep) const
{
	LPCWSTR lp_sz_pat = _T("%s=%s");
	CStringW cs_pat ;
	CStringW cs_args;
	if (m_args.empty())
		return (cs_args = _T("command line has no argument;"));

	for (TCmdLineArgs::const_iterator it_ = m_args.begin(); it_ != m_args.end(); ++it_)
	{
		cs_pat.Format(
				lp_sz_pat, (LPCWSTR)it_->first, (LPCWSTR)it_->second
			);
		cs_args += cs_pat;
		cs_args +=(NULL == _lp_sz_sep ? _T("; ") : _lp_sz_sep);
	}
	return cs_args;
}

/////////////////////////////////////////////////////////////////////////////

CArgument:: CArgument (void) : m_type(0) {}
CArgument:: CArgument (const CArgument& _arg) : CArgument() { *this = _arg; }
CArgument:: CArgument (const WORD _w_res_name, const WORD _w_res_verb, const DWORD _dw_type) : CArgument() {
	this->Name(_w_res_name); 
	this->Verb(_w_res_verb);
	this->Type(_dw_type);
}
CArgument:: CArgument (LPCWSTR _lp_sz_name, LPCWSTR _lp_sz_verb, const DWORD _dw_type) : m_name(_lp_sz_name), m_verb(_lp_sz_verb), m_type(_dw_type) {}
CArgument::~CArgument (void) {}

/////////////////////////////////////////////////////////////////////////////

bool      CArgument::Is  (void) const { return (m_name.IsEmpty() == false); }
LPCWSTR   CArgument::Name(void) const { return  m_name.GetString(); }
HRESULT   CArgument::Name(const WORD _w_res_id) {
	if (0 == _w_res_id)
		return E_INVALIDARG;

	m_name.LoadStringW(_w_res_id);
	return (m_name.IsEmpty() ? __DwordToHresult(ERROR_RESOURCE_NOT_PRESENT) : S_OK);
}
HRESULT   CArgument::Name(LPCWSTR _lp_sz_name ) {
	if (NULL == _lp_sz_name || 0 == ::lstrlenW(_lp_sz_name))
		return E_INVALIDARG;

	m_name = _lp_sz_name;
	return S_OK;
}
DWORD     CArgument::Type(void) const { return m_type; }
bool      CArgument::Type(const DWORD _type) { const bool b_changed = m_type != _type; m_type = _type; return b_changed; }
LPCWSTR   CArgument::Verb(void) const { return m_verb.GetString(); }
HRESULT   CArgument::Verb(const WORD _w_res_id) {
	if (0 == _w_res_id)
		return E_INVALIDARG;

	m_verb.LoadStringW(_w_res_id);
	return (m_verb.IsEmpty() ? __DwordToHresult(ERROR_RESOURCE_NOT_PRESENT) : S_OK);
}
HRESULT   CArgument::Verb(LPCWSTR _lp_sz_desc ) {
	if (NULL == _lp_sz_desc || 0 == ::lstrlenW(_lp_sz_desc))
		return E_INVALIDARG;

	m_verb = _lp_sz_desc;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CArgument& CArgument::operator = (const CArgument& _ref) {
	this->Name(_ref.Name());
	this->Verb(_ref.Verb());
	this->Type(_ref.Type());
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

bool CArgument::operator == (const CArgument& _ref) const { return (this->Type() == _ref.Type()); }
bool CArgument::operator == (LPCWSTR _lp_sz_name) const {
	if (NULL == _lp_sz_name || 0 == ::lstrlenW(_lp_sz_name))
		return false;
	else
		return (0 == m_name.CompareNoCase(_lp_sz_name));
}