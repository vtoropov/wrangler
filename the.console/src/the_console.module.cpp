/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 on 10:23:21p, UTC+7, Phuket, Rawai, Friday;
	This is sound-bin-trans receiver desktop console application entry point file.
	-----------------------------------------------------------------------------
	Adopted to Wanderer Test project on 6-Jan-2021 at 11:26:41.354 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "the_console.h"
#include "the_console.cmd.ln.h"

#include "stroller_iface.h"
#include "dump.data.h"
/////////////////////////////////////////////////////////////////////////////

namespace wanderer { namespace test { namespace client {

}}}
using namespace wanderer::test::client;
/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID) {
	INT n_res = 0;
	static LPCWSTR lp_sz_sep = _T("\n\t\t"); lp_sz_sep;  // not used;
#if _WIN64
	_out.OnCreate(_T("Wanderer Test Project Console [x64]"), _T("Calculating an Access of Surface"));
#else
	_out.OnCreate(_T("Wanderer Test Project Console [x32]"), _T("Calculating an Access of Surface"));
#endif

	TCmdLine cmd_ln;

	TAvailArea surface;

	// initial test input data: digit sum = 25; x = 1000; y = 1000;
	const POINT pt_entry = {
		cmd_ln.Arg(_T("x"), 1000), cmd_ln.Arg(_T("y"), 1000)
	};
	const ULONG ul_sum = cmd_ln.Arg(_T("z"), 25);

	CStringW cs_msg; cs_msg.Format(_T("Entry point: { x=%d; y=%d; z=%d; }"), pt_entry.x, pt_entry.y, ul_sum);
	_out <<  cs_msg;
	_out <<_T("Calculating extra ranges of surface...");

	HRESULT hr_ = surface.Calc_Extrema(pt_entry.x, pt_entry.y, ul_sum);
	if (SUCCEEDED(hr_)) {
		surface.Predict() = ul_sum;
#if defined(_DEBUG)
		cs_msg.Format(_T("Result is\n\n\t\tx-range=%s\n\t\ty-range=%s\n"), (LPCWSTR)surface.Horz().Print(), (LPCWSTR)surface.Vert().Print());
		_out << cs_msg;
#endif
		_out <<_T("Getting available area...");
		const ULONG u_area = surface.Area();
		if (false == surface.Error().Is()) {
			cs_msg.Format(_T("Available area is:\n\n\t\t%s\n"), surface.Print().GetString());
			_out << cs_msg;

			cs_msg.Format(
				_T(
					"\n\n\t\t"
					"The area is a set of available coordinate nodes;\n\t\t"
					"The ant will be able to take not less than %u steps and its legs will fall off;"
					"\n"),
				u_area - 1
			);
			_out << cs_msg;
#if defined(_use_it)
			TDumper dump; dump.Path(_T("e:\\wanderer_test.txt"));
			dump << surface.Horz(); 
			dump << surface.Vert();
			dump << surface;
			TDumper dump_err; dump_err.Path(_T("e:\\wanderer_errors.txt"));
			dump_err.Errors(surface, ul_sum);

			const UINT n_totals = dump_err.Total();
			const UINT n_errors = dump_err.Errors();

			cs_msg.Format(_T("Error dump: total=%u; errors=%u;"), n_totals, n_errors);
			_out << cs_msg;
#else
			CStringW cs_path = cmd_ln.Arg(_T("csv"));
			if (cs_path.IsEmpty() == false) {
				TDumper dump; dump.Path((LPCWSTR)cs_path);
				hr_ = dump.To_Csv(surface);
				if (FAILED(hr_)){
					shared::sys_core::CError err_; err_ << hr_ = _T("Error to dump to CSV file;");
					_out << err_;
				}
			}
#endif
		} 
	}
	if (surface.Error().Is()) {
		shared::sys_core::CError err_; err_ << surface.Error().Result() = surface.Error().Desc();
		_out << err_;
	}


	_out.OnWait(_T("Press any key or [x] close button for exiting the console;"));
	return n_res;
}