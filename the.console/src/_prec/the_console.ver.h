#ifndef _SNDRCVCONVER_H_6ECEFB93_B41F_4677_85B0_CEFB85902C6D_INCLUDED
#define _SNDRCVCONVER_H_6ECEFB93_B41F_4677_85B0_CEFB85902C6D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 10:10:30p, UTC+7, Phuket, Rawai, Friday;
	This is sound-bin-trans receiver desktop console application version declaration file.
	-----------------------------------------------------------------------------
	Adopted to Wanderer Test project on 6-Jan-2021 at 11:10:35.893 am, UTC+7, Novosibirsk, Tuesday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE              // Specifies that the minimum required platform is Internet Explorer 9.0 (Windows Vista, service pack 2).
#define _WIN32_IE      0x0900  // Change this to the appropriate value to target other versions of IE.
#endif

#endif/*_SNDRCVCONVER_H_6ECEFB93_B41F_4677_85B0_CEFB85902C6D_INCLUDED*/