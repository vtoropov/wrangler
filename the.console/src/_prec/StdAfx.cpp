/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 10:14:02p, UTC+7, Phuket, Rawai, Friday;
	This is sound-bin-trans receiver desktop console application precompiled header creation file.
	-----------------------------------------------------------------------------
	Adopted to Wanderer Test project on 6-Jan-2021 at 11:17:01.792 am, UTC+7, Novosibirsk, Tuesday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)