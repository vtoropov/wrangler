#ifndef _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
#define _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Feb-2019 at 6:52:05a, UTC+7, Novosibirsk, Sunday;
	This is FIX Engine shared library configuration command line interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Wanderer test project on 6-jan-2021 at 11:51:36.270 am, UTC+7, Novosibirsk, Tuesday;
*/
#include <shellapi.h>
#include "shared.sys.err.h"
namespace wanderer { namespace test { namespace client {

	typedef ::std::map<::ATL::CStringW, ::ATL::CStringW> TCmdLineArgs;

	class CCommandLine {
	private:
		CStringW     m_module_full_path;
		TCmdLineArgs m_args;
	public:
		 CCommandLine(void);
		~CCommandLine(void);
	public:
		HRESULT      Append(LPCWSTR _lp_sz_nm, LPCWSTR _lp_sz_val);
		CStringW     Arg   (LPCWSTR _lp_sz_nm) const;
		LONG         Arg   (LPCWSTR _lp_sz_nm, const LONG _def_val)const;
		TCmdLineArgs Args  (void)const;                                  // returns a copy of command line argument collection;
		VOID         Clear (void)     ;
		INT          Count (void)const;
		bool         Has   (LPCWSTR pszArgName)const;
		CStringW     ModuleFullPath(void)const;
		CStringW     ToString (LPCWSTR _lp_sz_sep = NULL) const;
	};

	class CArgument {
	protected:
		DWORD     m_type;
		CStringW  m_name;
		CStringW  m_verb;

	public:
		 CArgument (void);
		 CArgument (const CArgument&);
		 CArgument (const WORD _w_res_name, const WORD _w_res_verb, const DWORD _dw_type);
		 CArgument (LPCWSTR _lp_sz_name, LPCWSTR _lp_sz_verb, const DWORD _dw_type);
		~CArgument (void);

	public:
		bool      Is  (void) const;
		LPCWSTR   Name(void) const;
		HRESULT   Name(const WORD _w_res_id);
		HRESULT   Name(LPCWSTR _lp_sz_name );
		DWORD     Type(void) const;
		bool      Type(const DWORD);
		LPCWSTR   Verb(void) const;
		HRESULT   Verb(const WORD _w_res_id);
		HRESULT   Verb(LPCWSTR _lp_sz_desc );

	public:
		CArgument& operator = (const CArgument&);

	public:
		bool operator == (const CArgument&) const;
		bool operator == (LPCWSTR _lp_sz_name) const;
	};
}}}

typedef ::std::vector<wanderer::test::client::CArgument> TArguments;

typedef wanderer::test::client::CCommandLine TCmdLine;

#endif/*_SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED*/