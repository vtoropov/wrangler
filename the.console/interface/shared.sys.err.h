#ifndef _SHAREDGENERROR_H_064B9394_D422_429C_A390_948FDB26D4BC_INCLUDED
#define _SHAREDGENERROR_H_064B9394_D422_429C_A390_948FDB26D4BC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-May-2012 at 9:39:48pm, GMT+3, Rostov-on-Don, Tuesday;
	This is Pulsepay Server Application Common Error Trace class declaration file;
	-----------------------------------------------------------------------------
	Adopted to FakeGPS driver project on 13-Dec-2019 at 10:19:37a, UTC+7, Novosibirsk, Friday;
	Adopted to Wanderer test project on 6-Jan-2021 at 12:18:17.522 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "shared.sys.sync.h"

#if defined(_UNICODE)
#define __MODULE__  __FUNCTIONW__
#else
#define __MODULE__  __FUNCTION__
#endif

#ifndef __DwordToHresult
#define __DwordToHresult(_word)  HRESULT_FROM_WIN32(_word)
#endif
#ifndef __LastErrToHresult
#define __LastErrToHresult()  __DwordToHresult(::GetLastError())
#endif

namespace shared { namespace sys_core {

	class CError {
	protected:
		HRESULT     m_result;
		CStringW    m_desc;
		CStringW    m_module;
		CSyncObject m_lock;

	public:
		 CError(void);
		~CError(void);

	public:
		DWORD       Code  (void) const ;
		VOID        Code  (const DWORD);
		LPCWSTR     Desc  (void) const ;
		CStringW    Format(LPCWSTR lp_sz_sep=NULL) const; // get formatted string, a separator is acceptable;
		LPCWSTR     Module(void) const ;      // gets module name that produces the error, if any;
		VOID        Module(LPCWSTR)    ;      // sets module name that produces the error;
		HRESULT     Result(void) const ;
		VOID        Result(const HRESULT);
		DWORD       Show  (const HWND  = ::GetActiveWindow()) const;
		VOID        State (const DWORD = ::GetLastError(), LPCWSTR _lp_sz_desc = NULL);
		VOID        State (HRESULT _err_code, LPCWSTR _lp_err_desc);
		bool        State (void) const;

	public:
		CError& operator << (const HRESULT);  // sets error result; S_OK is acceptable;
		CError& operator << (LPCWSTR);        // sets error module;
		CError& operator =  (const DWORD  );  // sets error result from win 32 error code;
		CError& operator =  (const HRESULT);  // sets error result; S_OK is acceptable;
		CError& operator =  (LPCWSTR);        // sets error description;
		CError& operator =  (const CError&);  // copies error object;
	public:
		operator const bool (void) const;     // returns true if error object is in error state, otherwise false;
		operator HRESULT(void) const;         // returns error result;
		operator LPCWSTR(void) const;         // returns error description;
	};

	bool operator==(const bool _lhs, const CError& _rhs);
	bool operator!=(const bool _lhs, const CError& _rhs);
	bool operator==(const CError& _lhs, const bool _rhs);
	bool operator!=(const CError& _lhs, const bool _rhs);

	bool operator==(const CError& _lhs, const CError& _rhs);
	bool operator!=(const CError& _lhs, const CError& _rhs);

}}

typedef const shared::sys_core::CError&   TErrorRef;

#endif/*_SHAREDGENERROR_H_064B9394_D422_429C_A390_948FDB26D4BC_INCLUDED*/