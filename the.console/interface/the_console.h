#pragma once
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Sep-2020 at 6:54:34p, UTC+7, Novosibirsk, Thursday;
	This is FakeGPS driver version 2 desktop client console interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Wanderer test project on 6-Jan-2021 at 11:42:05.633 am, UTC+7, Novosibirsk, Tuesday;
*/
namespace wanderer { namespace test { namespace client {

	class CConsole_simplified {
	private:
		ATL::CWindow m_wnd;

	public:
		 CConsole_simplified (void);
		~CConsole_simplified (void);

	public:
		HRESULT  OnCreate (LPCWSTR _lp_sz_cap , LPCWSTR _lp_sz_title);
		HRESULT  OnWait   (LPCWSTR _lp_sz_msg);
		CWindow& Window   (void);

	public:
		CConsole_simplified& operator <<(LPCWSTR _lp_sz_info); // prints out an information;
		CConsole_simplified& operator >>(LPCWSTR _lp_sz_err ); // prints out an error details;
		CConsole_simplified& operator <=(LPCWSTR _lp_sz_warn); // prints out an warning;
	};

}}}

typedef wanderer::test::client::CConsole_simplified out_;

/////////////////////////////////////////////////////////////////////////////

static out_ _out;